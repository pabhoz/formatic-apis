import { Injectable, Inject } from '@angular/core';
import { SESSION_STORAGE, StorageService as Storage } from 'angular-webstorage-service';


@Injectable({
  providedIn: 'root'
})
export class StorageService {

  saved = {
    artists: [],
    lyrics: [],
    tracks: []
  };
  constructor(@Inject(SESSION_STORAGE) private storage: Storage) { }

  public save(obj,key): void {
    this.saved[key] = this.storage.get(key) || [];
    this.saved[key].push(obj);
    this.storage.set(key, this.saved[key]);
  }
  
}
