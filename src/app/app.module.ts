import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//Módulo para habilitar las peticiones http
import { HttpModule } from '@angular/http';
//Módulo de cliente para usar httl
import { HttpClientModule } from '@angular/common/http';
import { StorageServiceModule } from 'angular-webstorage-service';

import { AppComponent } from './app.component';
import { ArtistComponent } from './artist/artist.component';
import { ArtistDetailComponent } from './artist-detail/artist-detail.component';
import { LyricsComponent } from './lyrics/lyrics.component';
import { TrackComponent } from './track/track.component';
import { TrackDetailComponent } from './track-detail/track-detail.component';
import { ArtistService } from './services/provider';
import { StorageService } from './services/storage.service';

@NgModule({
  declarations: [
    AppComponent,
    ArtistComponent,
    ArtistDetailComponent,
    LyricsComponent,
    TrackComponent,
    TrackDetailComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    StorageServiceModule
  ],
  providers: [
    ArtistService,
    StorageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
